/**
 * Color mixer.
 * @module get and post
 */


var bodyParser = require('body-parser');

var search = [{line: 'no data found'}];

var fs = require('fs');

var urlencodedParser = bodyParser.urlencoded({extended: true});

module.exports = function(app){
  app.use(bodyParser.json());

  /**
   * Route serving view search.
   * @name get/
   * @function
   * @memberof module:exports/
   * @inner
   * @param {string} path - Express path
   * @param {callback} middlewear - Express middlewear.
   */
  app.get('/', function(req, res){
    //set default variable
    var pageSize = 10;
    var totalLogs = search.length;
    var currentPage = 1;
    var pageCount = totalLogs/pageSize;
    var logArrays = [];
    var logList = [];

    //split list into group
    if(search !== 'undefined' && search.length > 0){
      while(search.length > 0){
        logArrays.push(search.splice(0, pageSize));
      }

      //set current page if specified as get variable
      if(typeof req.query.page !== 'undefined'){
        currentPage = +req.query.page;
      }

      logList = logArrays[+currentPage - 1];

      res.render('list', {
          logs: logList,
          pageSize: pageSize,
          totalLogs: totalLogs,
          pageCount: pageCount,
          currentPage: currentPage
      });
    }else{
      search = [{line: 'no data found'}];
      res.render('list', search);
    }
  });

  /**
   * Route serving view search.
   * @name post/
   * @function
   * @memberof module:exports/
   * @inner
   * @param {string} path - Express path
   * @param {callback} middlewear - Express middlewear.
   */
  app.post('/', urlencodedParser, function(req, res){
    console.log(req.body.path);
    var strpath = req.body.path;
    var array = fs.readFileSync(strpath).toString().split("\n");

    search = copyArray(array);
    app.set('search', search);
    res.json(search);

    console.log(search[1]);
  });


  function copyArray(array){
    var temp = [];
    for(i in array){
      temp.push({line: array[i]});
    }

    return temp;
  }


};
