/**
 * @file Eunomia's main file and entry point
 * @author Indra Rahmat
 * @exports server
 */

 //external packages
var express = require('express');

//internal packages
var controller = require('./controllers/logController');

//initiate app variable
var app = express();

//set up template engine
app.set('views', 'views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

//static files
app.use(express.static('./public'));

//fire controller
controller(app);

//listen to port
app.listen(3001);
console.log('Your are listening port 3001');
