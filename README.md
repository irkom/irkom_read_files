# README #
irkom read files is simple web application developed with nodejs, express, body-parser, ejs, chai, mocha and jsdocs.

### How do I run this app locally?
1. Clone this repository
1. run this command on your console
  1. > $ npm install
1. run the server locally
  1. > $ node server.js
  1. Open your browser and enter (http://localhost:3001)
1. the jsDocs is ready to open in folder "out", just click index.html